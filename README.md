# OATs'inside

OATs'inside is an analysis tool that handles native Android
applications. The system uses a hybrid approach based on dynamic
monitoring and trace-based symbolic execution to output control flow
graphs (CFGs) for each method of the analyzed application.

The resources associated to this tool are:

- This [repository](https://gitlab.inria.fr/cidre-public/oatinside/oatinside) which contains pre-compiled binaries for a Sony Xperia X (F5121) smartphone.

- The source code of the dynamic part of OATs'inside, split in four repositories: the [runtime](https://gitlab.inria.fr/cidre-public/oatinside/oatinside_runtime), the [kernel](https://gitlab.inria.fr/cidre-public/oatinside/oatinside_kernel), the [base framework](https://gitlab.inria.fr/cidre-public/oatinside/oatinside_framework) and the [protobuf library](https://gitlab.inria.fr/cidre-public/oatinside/oatinside_protobuf).

- The source code of the trace-based symbolic execution and the CFG creator [here](https://gitlab.inria.fr/cidre-public/oatinside/oatinside-host-scripts).

- [Datasets](https://gitlab.inria.fr/cidre-public/oatinside/oatinside_dataset) used in the article.

## Usage

1. Flash the [pre-compiled binaries](/pre-compiled-binaries/). To re-compile them, see the `Compilation` section.

2. Retrieve the userId of the apk you want to analyse using:
```bash
adb shell dumpsys <apk package name> | grep userId
```

3. Append this userId in the file `/data/local/tmp/traced_uid`. Each line of this file should be a UID.

4. (For dumping) Put in the file `/data/local/tmp/dumped_uid`, the userId of the APK and the length of the method name that should be dumped one the first line and the method name in the second line.
   Create the folder that will receive the dumps using `adb shell mkdir /data/local/tmp/dumped_mem/`.

5. Retrieve the events on the host using `python2 receive_events.py`. Note that OATs'inside runtime connect to localhost port 4242, so this port has to be forwarded to the host using `adb reverse tcp:4242 tcp:4242` for example.

6. (For dumping) You need to retrieve the memory dumped that are located in `adb pull /data/local/tmp/dumped_mem/dump_no_*`.

7. Create a CFG using `python2 create_graph_from_event_file.py <event_file>`.

8. (If dumping has been made) Retrieve conditions using `python3 retrieve_conditions_from_dump.py <dump_file> <event_file> <method_addr>`. The `dump_file` corresponds to the path of the dumped files without the final number (for example, `path/dump_no_1` and `path/dump_no_2` becomes `path/dump_no_`).

## Compilation

This git repository contains the local manifest file that should be
used to compile AOSP for Xperia X Nougat 7.0. The compilation guide
made by sony (which does not include modifications proposed by
OATs'inside) is available at
[build_aosp_nougat_7.0](https://developer.sony.com/develop/open-devices/guides/aosp-build-instructions/build-aosp-nougat-7-0).

The added local manifest changes 4 git projects:

- patch for the Android runtime (art) library, located at 
  [oatinside_runtime](https://gitlab.inria.fr/cidre-public/oatinside/oatinside_runtime).
  
- patch for the kernel, located at
  [oatinside_kernel](https://gitlab.inria.fr/cidre-public/oatinside/oatinside_kernel).
  
- patch for the "base" framework, located at
  [oatinside_framework](https://gitlab.inria.fr/cidre-public/oatinside/oatinside_framework).

- patch (compatibility with the runtime compilation) for the protobuf library, located at 
  [oatinside_protobuf](https://gitlab.inria.fr/cidre-public/oatinside/oatinside_protobuf).

To use theses patches (and thus compiling OATs'inside), you need to
follow the steps given by sony. During the step "Initialise the AOSP
Tree", **BEFORE** synchronizing the AOSP repo (command `repo sync`),
you need the execute the following command in the root directory of
the repo. This command adds the manifest file at the right place with
the right name.

```bash
mkdir -p .repo/local_manifests && git archive --remote=https://gitlab.inria.fr/cidre-public/oatinside/oatinside.git HEAD | tar -C .repo/local_manifests --transform='s/^/ZZ/' -x detect_packers.xml
```

Note: The manifest file name needs to be alphabetically greater than
the names of the local manifests containing projects that are
replaced, that is, in the Xperia X Nougat 7.0 case, greater than
"LA.BR.1.3.3_rb2.14.mxl". This is performed by `tar` using the
`--transform` option.

Additionally, since new syscalls are added to the kernel, you need to declare them to the libc (bionic in AOSP) before the compilation:

- add the new syscalls declaration in the file `bionic/libc/SYSCALLS.TXT` by adding the following lines:
```java
long tracing(void) all
long unsafe_stop_other_threads(bool) all
```

- launch the following commands in the root directory of the repo. Their behavior are described in the file [bionic/libc/kernel/README.TXT](https://android.googlesource.com/platform/bionic/+/refs/heads/nougat-mr0.5-release/libc/kernel/README.TXT).
```bash
./bionic/libc/kernel/tools/generate_uapi_headers.sh --use-kernel-dir kernel/sony/msm/
python2 bionic/libc/kernel/tools/update_all.py
python2 bionic/libc/tools/gensyscalls.py
```

- add definitions of `__NR_tracing`, `SYS_TRACING`, `__NR_unsafe_stop_other_threads`, `SYS_unsafe_stop_other_threads` in the headers of the compiler (prebuilts/gcc/...) that you use.

## Note on source code

### Runtime

Implements almost all of the runtime logic of OATs'inside. Main files to look are: `signal_manager.cc`, `signal_manager.h`, `probe_manager.cc` and `probe_manager.h`. The signal manager retrieves and generates all the low level signals (SEGV and TRAP). The probe manager is called whenever a bytecode event is detected and then logs them using protobuf.

### Kernel

Add, for each task, a new ASID (Address Space IDentifier) that is used by OATs'inside to unprotect heap memory pages. The corresponding kernel option is `NO_PARALLEL_PERMISSIONS` and `PGTABLES_PARALLEL_PERMISSIONS`. The implementation is only down for `arm64`.

Additionally, 4 new syscalls are introduced:
- `pp_switch_perms`: select the ASID of the calling task.
- `pp_mprotect_b`: calls mprotect in the new ASID.
- `tracing`: declares OATs'inside SEGV and TRAP handler addresses so if the application tries to change them the kernel can still call them.
- `unsafe_stop_other_threads`: stops all the threads (except the calling thread) that are in process of the calling thread. Used to stop an application when dumping the memory.

### Base framework

Timeout when running the application have been increased to prevent Android from stopping the application when OATs'inside dumps the whole memory.

### Protobuf modification

Few compilation issues have been patched.

 
